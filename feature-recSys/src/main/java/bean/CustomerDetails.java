package bean;

public class CustomerDetails {

	private int customer,segment,region;
	
	public int getcustomer() {
		return customer;
	}

	public void setcustomer(int customer) {
		this.customer = customer;
	}
	
	public int getSegment() {
		return segment;
	}

	public void setSegment(int segment) {
		this.segment = segment;
	}
	
	public int getRegion() {
		return region;
	}

	public void setRegion(int region) {
		this.region = region;
	}

	
}
