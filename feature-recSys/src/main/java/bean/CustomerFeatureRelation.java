package bean;

import java.sql.Date;

//import java.time.format.DateTimeFormatter;

public class CustomerFeatureRelation {

	int customer,feature;
	
	Date lastuse;
	
	double frequencyuse;
	
	public int getCustomer(){
		return customer;
	}
	
	public void setcustomer(int customer) {
		this.customer = customer;
	}
	
	public int getFeature() {
		return feature;
	}

	public void setFeature(int feature) {
		this.feature = feature;
	}
	
	public Date getLastUse(){
		return lastuse;
	}
	
	public void setLastUse(Date lastuse){
		this.lastuse = lastuse;
	}
	
	
	public double getFrequencyUse(){
		return frequencyuse;
	}
	
	public void setFrequencyUse(double frequencyuse){
		this.frequencyuse = frequencyuse;
	}
	
}
