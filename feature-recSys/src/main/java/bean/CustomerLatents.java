package bean;

import org.apache.spark.mllib.linalg.Vector;

public class CustomerLatents {

	private long customer;
	private Vector customer_latents;
	
	public long getcustomer() {
		return customer;
	}

	public void setcustomer(long customer) {
		this.customer = customer;
	}
	
	public Vector getLatents() {
		return customer_latents;
	}

	public void setLatents(Vector customer_latents) {
		this.customer_latents = customer_latents;
	}
}
