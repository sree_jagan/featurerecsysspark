package bean;

public class FMeasure {
	private double completeness;
	private double fmeasure;
	
	public double getCompleteness(){
		return completeness;
	}
	
	public void setCompletness(double completeness){
		this.completeness = completeness;
	}
	public double getFMeasure(){
		return fmeasure;
	}
	
	public void setFMeasure(double fmeasure){
		this.fmeasure = fmeasure;
	}
}
