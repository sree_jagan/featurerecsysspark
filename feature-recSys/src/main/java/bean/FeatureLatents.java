package bean;

import java.util.List;

import org.apache.spark.mllib.linalg.Vector;

public class FeatureLatents {

	private long feature;
	private List<Double> feature_latents;
	
	public long getfeature() {
		return feature;
	}

	public void setfeature(long feature) {
		this.feature = feature;
	}
	
	public List<Double> getLatents() {
		return feature_latents;
	}

	public void setLatents(List<Double> feature_latents) {
		this.feature_latents = feature_latents;
	}
}
