package bean;

public class LatentFactors {

	private long label,latent_index;
	private double value;
	
	public long getlabel() {
		return label;
	}

	public void setlabel(long label) {
		this.label = label;
	}
	
	public long getLatentIndex() {
		return latent_index;
	}

	public void setLatentIndex(long latent_index) {
		this.latent_index = latent_index;
	}
	
	public double getValue() {
		return value;
	}

	public void setValue(double value) {
		this.value = value;
	}
}
