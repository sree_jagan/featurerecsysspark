package bean;

import org.apache.spark.mllib.linalg.Vector;

public class SgdUpdate {

	private double customer,feature,original_error,updated_error,user_bias,item_bias,rating;
	private Vector customer_factors,feature_factors;
	
	public double getcustomer() {
		return customer;
	}

	public void setcustomer(double customer) {
		this.customer = customer;
	}
	public double getfeature() {
		return feature;
	}

	public void setfeature(double feature) {
		this.feature = feature;
	}
	public double getOriginalError() {
		return original_error;
	}

	public void setOriginalError(double original_error) {
		this.original_error = original_error;
	}
	public double getUpdatedError() {
		return updated_error;
	}

	public void setUpdatedError(double updated_error) {
		this.updated_error = updated_error;
	}
	public double getUserBias() {
		return user_bias;
	}

	public void setUserBias(double user_bias) {
		this.user_bias = user_bias;
	}
	public double getItemBias() {
		return item_bias;
	}

	public void setItemBias(double item_bias) {
		this.item_bias = item_bias;
	}
	public double getRating() {
		return rating;
	}

	public void setRating(double rating) {
		this.rating = rating;
	}
	public Vector getCustomerFactors() {
		return customer_factors;
	}

	public void setCustomerFactors(Vector customer_factors) {
		this.customer_factors = customer_factors;
	}
	public Vector getFeatureFactors() {
		return feature_factors;
	}

	public void setFeatureFactors(Vector feature_factors) {
		this.feature_factors = feature_factors;
	}
}
