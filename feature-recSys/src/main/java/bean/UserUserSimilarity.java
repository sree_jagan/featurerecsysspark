package bean;

public class UserUserSimilarity {
	private int userId1,userId2;
	private double similarity;
	
	public int getUserID1(){
		return userId1;
	}
	
	public void setUserID1(int userId1){
		this.userId1 = userId1;
	}
	
	public int getUserID2(){
		return userId2;
	}
	
	public void setUserID2(int userId2){
		this.userId2 = userId2;
	}
	
	public double getUsersSimilarity(){
		return similarity;
	}
	
	public void setUsersSimilarity(double similarity){
		this.similarity = similarity;
	}
}
