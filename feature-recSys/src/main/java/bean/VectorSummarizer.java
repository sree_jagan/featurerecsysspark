package bean;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.stat.MultivariateOnlineSummarizer;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.expressions.Aggregator;

import scala.Tuple2;
import breeze.linalg.DenseVector;


//MultivariateOnlineSummarizer Summarizer = new MultivariateOnlineSummarizer();
public abstract class VectorSummarizer extends Aggregator<Row,MultivariateOnlineSummarizer,Vector> {
	
}