package impl;


import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileUtil;
import org.apache.hadoop.fs.Path;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.mllib.linalg.BLAS;
import org.apache.spark.mllib.linalg.Vector;
import org.apache.spark.mllib.linalg.VectorUDT;
import org.apache.spark.mllib.linalg.Vectors;
import org.apache.spark.mllib.stat.MultivariateOnlineSummarizer;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.functions;
import org.apache.spark.sql.expressions.MutableAggregationBuffer;
import org.apache.spark.sql.expressions.UserDefinedAggregateFunction;
import org.apache.spark.sql.types.DataType;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import scala.Tuple2;

public class BrisMF implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static StructType rating_schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("customer", DataTypes.LongType, false),
					DataTypes.createStructField("feature", DataTypes.LongType, false),
					DataTypes.createStructField("rating", DataTypes.DoubleType, true) });
	static StructType userbias_schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("customer", DataTypes.LongType, false),
					DataTypes.createStructField("bias", DataTypes.DoubleType, true) });
	static StructType itembias_schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("item", DataTypes.LongType, false),
					DataTypes.createStructField("bias", DataTypes.DoubleType, true) });
	static StructType customer_latent_schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("customer", DataTypes.LongType, false),
					DataTypes.createStructField("latent_index", DataTypes.LongType, false),
					DataTypes.createStructField("value", DataTypes.DoubleType, true) });
	static StructType prediction_schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("customer", DataTypes.DoubleType, false),
					DataTypes.createStructField("feature", DataTypes.DoubleType, false),
					DataTypes.createStructField("prediction", DataTypes.DoubleType, true) });
	static StructType latent_customer_schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("customer", DataTypes.DoubleType, false),
					DataTypes.createStructField("latent_customer", new VectorUDT(), false) });
	static StructType latent_feature_schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("feature", DataTypes.DoubleType, false),
					DataTypes.createStructField("latent_feature", new VectorUDT(), false) });
	static StructType latent_feature_average = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("feature", DataTypes.DoubleType, false),
					DataTypes.createStructField("latent_feature", new VectorUDT(), false),
					DataTypes.createStructField("feature_weight", DataTypes.DoubleType, false)});
	static StructType Fmeasure_schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("customer", DataTypes.DoubleType, false),
					DataTypes.createStructField("f-measure", DataTypes.DoubleType, false) });
	static StructType updated_dataset_schema = DataTypes
			.createStructType(new StructField[] {
					DataTypes.createStructField("customer", DataTypes.DoubleType, false),
					DataTypes.createStructField("customer_latents", new VectorUDT(), false),
					DataTypes.createStructField("feature", DataTypes.DoubleType, false),
					DataTypes.createStructField("feature_latents", new VectorUDT(), false),
					DataTypes.createStructField("original_error", DataTypes.DoubleType, true),
					DataTypes.createStructField("updated_error", DataTypes.DoubleType, true),
					DataTypes.createStructField("user_bias", DataTypes.DoubleType, true),
					DataTypes.createStructField("item_bias", DataTypes.DoubleType, true),
					DataTypes.createStructField("rating", DataTypes.DoubleType, false)
					//DataTypes.createStructField("prediction", DecimalType.apply(10, 10), false)
					});
final static double learnRate = 0.0034;
final static double regParam = 0.001;
final static int latent_features = 20;
public static class VectorSummarizer extends UserDefinedAggregateFunction{
	/**
}
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private StructType inputSchema;
    private StructType bufferSchema;
    MultivariateOnlineSummarizer summarizer = new MultivariateOnlineSummarizer();

    public VectorSummarizer() {
      List<StructField> inputFields = new ArrayList<StructField>();
      inputFields.add(DataTypes.createStructField("inputColumn", new VectorUDT() , true));
      inputSchema = DataTypes.createStructType(inputFields);

      List<StructField> bufferFields = new ArrayList<StructField>();
      //bufferFields.add(DataTypes.createStructField("sum", DataTypes.LongType, true));
      bufferFields.add(DataTypes.createStructField("count", new VectorUDT(), true));
      bufferSchema = DataTypes.createStructType(bufferFields);
    }
	
	
	@Override
	public StructType bufferSchema() {
		// TODO Auto-generated method stub
		return bufferSchema;
	}
	@Override
	public DataType dataType() {
		// TODO Auto-generated method stub
		return new VectorUDT();
	}
	@Override
	public boolean deterministic() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public Vector evaluate(Row r) {
		// TODO Auto-generated method stub
		Vector vec = (Vector)r.get(0);
		return vec;
	}
	public void initialize(MutableAggregationBuffer buffer) {
		// TODO Auto-generated method stub
		
		summarizer.add(Vectors.zeros(latent_features));
		buffer.update(0, summarizer.mean());
	}
	@Override
	public StructType inputSchema() {
		// TODO Auto-generated method stub
		return inputSchema;
	}
	public void merge(MutableAggregationBuffer buffer, Row buffer2) {
		// TODO Auto-generated method stub
		summarizer.add((Vector)buffer2.get(0));
		buffer.update(0, summarizer.mean());
	}
	public void update(MutableAggregationBuffer buffer1, Row input) {
		// TODO Auto-generated method stub
		summarizer.add((Vector)input.get(0));
		buffer1.update(0, summarizer.mean());
	}



	
}
	public double brisMF(RDD<Row> rating_matrix, SparkSession spark, final int customer) throws IOException{
		

		Dataset<Row> rating_dataset = spark.createDataFrame(rating_matrix, rating_schema );
		
		rating_dataset.createOrReplaceTempView("ratings");
		
		merge("/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/resources/rating_dataset","/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/resources/rating_dataset_merged.csv");
		final Dataset<Row> user_bias =  rating_dataset.groupBy(rating_dataset.col("customer")).avg("rating").withColumnRenamed("avg(rating)", "user_average");
		Dataset<Row> item_bias = rating_dataset.groupBy(rating_dataset.col("feature")).avg("rating").withColumnRenamed("avg(rating)", "item_average");
		user_bias.createOrReplaceTempView("user_bias");
		item_bias.createOrReplaceTempView("item_bias");
		final double total_average = spark.sql("select avg(rating) from ratings").first().getDouble(0);
		
		
		
		Dataset<Row> feature_by_target = spark.sql("select * from ratings where customer="+customer);
		feature_by_target.createOrReplaceTempView("feature_filtered");
		
		Dataset<Row> rating_data = rating_dataset.sample(false, 0.1);
		
		rating_data.createOrReplaceTempView("rating_data");
		
		Dataset<Row> customers_distinct = spark.sql("select distinct(customer) from ratings");
		final Dataset<Row> features_distinct = spark.sql("select distinct(feature) from ratings");

	
	RDD<Tuple2<Object,Vector>> P_rdd = customers_distinct.toJavaRDD()
						.map(new Function<Row,Tuple2<Object,Vector>>(){

							/**
							 * 
							 */
							private static final long serialVersionUID = 1L;

							public Tuple2<Object, Vector> call(Row v1)
								throws Exception {
								// TODO Auto-generated method stub
								double v[] = new double[latent_features];
								double label = Double.parseDouble(String.valueOf(v1.get(0)));
								for(int i=0;i<latent_features;i++){
									v[i] = (double)Math.round(Math.random()*100)/100;
								}
								Vector vec = Vectors.dense(v);
								return new scala.Tuple2<Object, Vector>(label,vec);
							}
													
						}).rdd();

		
	RDD<Row> P_dataset = P_rdd.toJavaRDD()
			.map(new Function<Tuple2<Object,Vector>,Row>(){

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public Row call(Tuple2<Object, Vector> v1) throws Exception {
					// TODO Auto-generated method stub
					double customer = ((Double)v1._1).doubleValue();
					//long cust = new BigDecimal(v1._1).longValue();
					Vector customer_feature = v1._2();
					Row r = RowFactory.create(customer,customer_feature);
					return r;
				}
				
			}).rdd();
	

	RDD<Tuple2<Object,Vector>> Q_rdd = features_distinct.toJavaRDD()
			.map(new Function<Row,Tuple2<Object,Vector>>(){

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public Tuple2<Object, Vector> call(Row v1)
					throws Exception {
					// TODO Auto-generated method stub
					double v[] = new double[latent_features];
					double label = Double.parseDouble(String.valueOf(v1.get(0)));
					for(int i=0;i<latent_features;i++){
						v[i] = (double)Math.round(Math.random()*100)/100;
					}
					Vector vec = Vectors.dense(v);
					return new scala.Tuple2<Object, Vector>(label,vec);
				}
										
			}).rdd();
	
	RDD<Row> Q_dataset = Q_rdd.toJavaRDD()
			.map(new Function<Tuple2<Object,Vector>,Row>(){

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				public Row call(Tuple2<Object, Vector> v1) throws Exception {
					// TODO Auto-generated method stub
				    double feature = ((Double)v1._1).doubleValue();
					Vector customer_feature = v1._2();
					Row r = RowFactory.create(feature,customer_feature);
					return r;
				}
				
			}).rdd();
	
	
	
	Dataset<Row> customer_latent_space = spark//.read().schema(latent_customer_schema).text("/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/resources/P_rdd/part-*");
			.createDataFrame(P_dataset, latent_customer_schema);
	customer_latent_space.createOrReplaceTempView("customer_latents_space");
	
	Dataset<Row> feature_latent_space = spark.createDataFrame(Q_dataset, latent_feature_schema);
	feature_latent_space.createOrReplaceTempView("feature_latents_space");
	
		
		Dataset<Row> merged_dataset = customer_latent_space.join(rating_data, customer_latent_space.col("customer").equalTo(rating_data.col("customer"))).drop(rating_data.col("customer"));
		Dataset<Row> complete_dataset1 = merged_dataset.join(feature_latent_space, merged_dataset.col("feature").$eq$eq$eq(feature_latent_space.col("feature"))).drop(merged_dataset.col("feature"))
									//.join(user_bias,merged_dataset.col("customer").$eq$eq$eq(user_bias.col("customer")))
									.withColumn("original_error", functions.lit("0.0").cast(DataTypes.DoubleType))
									.withColumn("updated_error", functions.lit("0.0").cast(DataTypes.DoubleType));;
									
		complete_dataset1.createOrReplaceTempView("complete_dataset1");
		Dataset<Row> complete_dataset = spark.sql("select cd.customer,cd.latent_customer,cd.feature,cd.latent_feature,cd.original_error,cd.updated_error,ub.user_average,ib.item_average,cd.rating from complete_dataset1 cd,user_bias ub,item_bias ib where cd.customer=ub.customer and cd.feature=ib.feature");
										//.withColumn("rating", functions.lit("1.0").cast(DataTypes.DoubleType));
		
		Dataset<Row> updated_dataset = spark.createDataFrame(factorUpdates(complete_dataset,customer_latent_space,feature_latent_space,learnRate,regParam,10,total_average,spark).rdd(),updated_dataset_schema);
		
		updated_dataset.show();
	
		updated_dataset.createOrReplaceTempView("updated_dataset");
		VectorSummarizer vector_mean = new VectorSummarizer();
		spark.udf().register("vector_mean", vector_mean);
		
		final Dataset<Row> updated_customer_latents = spark.sql("select customer,vector_mean(customer_latents) from updated_dataset where customer="+customer+" group by customer");
		Dataset<Row> updated_feature_latents = spark.sql("select feature,feature_latents from updated_dataset");
		updated_feature_latents.createOrReplaceTempView("updated_feature_latents");
		
		final Dataset<Row> features_for_recommendation = spark.sql("select feature,feature_latents from updated_feature_latents where feature not in (select feature from rating_data where customer="+customer+")");
		
		final double[] customer_latent_factors = ((Vector)updated_customer_latents.first().get(1)).toArray();
		

		features_for_recommendation.createOrReplaceTempView("feature_for_recommendation");

		Dataset<Row> for_recommendation = spark.sql("select feature,vector_mean(feature_latents) as feature_latents from feature_for_recommendation group by feature");
		
		for_recommendation.createOrReplaceTempView("for_recommendation");
		
		Dataset<Row> prediction_dataset = spark.sql("select fr.feature,fr.feature_latents,(fv.profit/fv.price) as weight from for_recommendation fr,feature_view fv where fr.feature=fv.feature");
		RDD<Row> predictions_mf = prediction_dataset.toJavaRDD()
									.map(new Function<Row,Row>(){

			/**
										 * 
										 */
										private static final long serialVersionUID = 1L;

			public Row call(Row value) throws Exception {
				// TODO Auto-generated method stub
				double target_customer = customer;
				double feature = value.getDouble(0);
				
				double[] feature_weights = ((Vector)value.get(1)).toArray();
				
				double feature_weight = Double.parseDouble(String.valueOf(value.get(2)));
				double prediction = 0.0;
				for(int i=0;i<feature_weights.length;i++){
					prediction = prediction+(customer_latent_factors[i]*feature_weights[i]);
					
				}
				Row r = RowFactory.create(target_customer,feature,prediction+feature_weight);
				return r;
			}
			
		}).rdd();
		Dataset<Row> predictions_MF = spark.createDataFrame(predictions_mf, prediction_schema);
		predictions_MF.createOrReplaceTempView("predictions_MF");
		Dataset<Row> recommended_features = spark.sql("select * from predictions_MF order by prediction desc limit 10");
		
		recommended_features.createOrReplaceTempView("final_predictions_MF");
		System.out.println("recommended_features="+recommended_features.count());
		Dataset<Row> real_data_test = rating_dataset.except(rating_data);
		real_data_test.createOrReplaceTempView("real_data_test");
		Dataset<Row> true_positives_MF = spark.sql("select * from final_predictions_MF where feature in (select feature from real_data_test where customer="+customer+")");
		double true_positive_features_MF = new BigDecimal(true_positives_MF.count()).doubleValue();
		
	  

		double Fmeasure_MF = 0.0;
		if(true_positive_features_MF>0){
				double precision_MF = true_positive_features_MF/10;
				double recall_MF = true_positive_features_MF/real_data_test.where(real_data_test.col("customer").$eq$eq$eq(customer)).count();
				Fmeasure_MF = (2*precision_MF*recall_MF)/(precision_MF+recall_MF);
		}else{
			Fmeasure_MF = 0.0;
		}
		System.out.print("true positives="+true_positive_features_MF+"related_set="+real_data_test.where(real_data_test.col("customer").$eq$eq$eq(customer)).count());
		return Fmeasure_MF;
	}
	
	private static void merge(String string, String string2) throws IOException {
		// TODO Auto-generated method stub
		//FileUtil.fullyDelete(new File(string));
		FileUtil.fullyDelete(new File(string2));
		Configuration hadoop_conf = new Configuration();
		FileSystem fs = FileSystem.get(hadoop_conf);
		FileUtil.copyMerge(fs, new Path(string), fs, new Path(string2), false, hadoop_conf, null);
		
	}





	private static Dataset<Row> factorUpdates(Dataset<Row> complete_dataset,final Dataset<Row> customer_latent_space,final Dataset<Row> feature_latent_space, final double learnRate, final double regParam, final int iterations,final double total_average, SparkSession spark) {
		// TODO Auto-generated method stubRDD<Row> update_vectors = complete_dataset.rdd().toJavaRDD()
		JavaRDD<Row> factor_updates = complete_dataset.toJavaRDD();
		
		for(int i=0;i<80;i++){
		
		factor_updates = factor_updates		
		.map(new Function<Row,Row>(){
		
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			//JavaRDD<CustomerLatents> latent_update_user = latent_update_customer.toJavaRDD();
			public Row call(Row v1) throws Exception {
				// TODO Auto-generated method stub
				final double customer = Double.parseDouble(String.valueOf(v1.get(0)));
				final double feature = Double.parseDouble(String.valueOf(v1.get(2)));
				double original_error = v1.getDouble(4);
				double updated_error = v1.getDouble(5);
				original_error = updated_error;
				
				double rating = v1.getDouble(8);
				Vector cust_latents = (Vector)v1.get(1);
				Vector feat_latents = (Vector)v1.get(3);
				//double dot_product = BLAS.dot(arg0, arg1)dot(cust_latents,fea_latents);
				double[] cus_latents = ((Vector)v1.get(1)).toArray();
				double[] fea_latents = ((Vector)v1.get(3)).toArray();
				double user_bias = v1.getDouble(6);
				double item_bias = v1.getDouble(7);
				
				double update_error = 0.0;
				update_error = (total_average+user_bias+item_bias+BLAS.dot(cust_latents, feat_latents));
				updated_error = (rating-update_error)*(rating-update_error);
				user_bias = (user_bias+(learnRate*updated_error - regParam*user_bias));
				
				item_bias = (item_bias+(learnRate*updated_error-regParam*item_bias));
				if(updated_error<=original_error){
				
				double[] customer_factors_array = new double[cus_latents.length];
				for(int k=0;k<cus_latents.length;k++){
					customer_factors_array[k] = cus_latents[k]+learnRate*(fea_latents[k]*updated_error-regParam*cus_latents[k]);
				}
				
				final Vector customer_latent_update = Vectors.dense(customer_factors_array);
				//BigDecimal update_factors_feature[] = new BigDecimal[feature_latents.length];
				double[] feature_factors_array = new double[fea_latents.length];
				for(int l=0;l<fea_latents.length;l++){
					feature_factors_array[l] = fea_latents[l]+learnRate*(cus_latents[l]*updated_error-regParam*fea_latents[l]);
					//System.out.print("feature_array   index="+l+"value="+feature_factors_array[l]);
					//feature_factors_array[l] = update_factors_customer[l].doubleValue();
				}
				
				final Vector feature_latent_update = Vectors.dense(feature_factors_array);
				Row r = RowFactory.create(customer,customer_latent_update,feature,feature_latent_update,original_error,updated_error,user_bias,item_bias,rating);
				return r;
				}else{
					Row r = RowFactory.create(customer,cust_latents,feature,feat_latents,original_error,updated_error,user_bias,item_bias,rating);
					
					return r;
				}
				
			}
		});
	}
			
		 Dataset<Row> updated_dataset = spark.createDataFrame(factor_updates, updated_dataset_schema);

			return updated_dataset;
	
	}
}
