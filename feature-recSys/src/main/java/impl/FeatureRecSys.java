/**
 * 
 */
package impl;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;
import org.apache.spark.rdd.RDD;

import org.apache.spark.sql.Dataset;

import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SparkSession;

import bean.CustomerDetails;
import bean.CustomerFeatureRelation;
import bean.FeatureDetails;


/**
 * @author rjsreeram91
 *
 */
public class FeatureRecSys implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	static SparkConf conf = new SparkConf()
			.setMaster("local[*]");
			//.set(spark.sql.crossJoin.enabled, "true");
	static SparkSession spark = SparkSession
			.builder()
			.appName("JavaKnnRecommender")
			.config(conf)
			.config("spark.sql.crossJoin.enabled", "true")
			.getOrCreate();
	static SparkContext sc = SparkContext.getOrCreate(conf);
	static JavaSparkContext jsc = JavaSparkContext.fromSparkContext(sc);


		

		  public static void main(String[] args) throws IOException {
			  //final int customer = 1224;
		   Dataset<CustomerDetails> dataset = spark
				   					.read()
				   					.option("header", true)
				   					.csv("src/main/resources/new_Clientes.csv")
		    						.as(Encoders.bean(CustomerDetails.class));
//				   					;
		   dataset.printSchema();
		   final Dataset<FeatureDetails> dataset2 = spark
					.read()
					.option("header", true)
					.csv("src/main/resources/new_ProdutosPreco.csv")
					.as(Encoders.bean(FeatureDetails.class));
	   
	   dataset2.printSchema();

	   dataset2.createOrReplaceTempView("feature_view");
	   final Dataset<CustomerFeatureRelation> dataset1 = spark
					.read()
					.option("header", true)
					.csv("src/main/resources/new_HistoricoVendas.csv")
					.as(Encoders.bean(CustomerFeatureRelation.class));
	   dataset.createOrReplaceTempView("customer");
		   dataset1.createOrReplaceTempView("customer_feature_relation");
//		   
//		   
		   Dataset<Row> sqlDF1 = spark.sql("select * from customer_feature_relation");
//	
//		 
		   
		   Dataset<CustomerFeatureRelation> dateDiff = sqlDF1.map(new MapFunction<Row,CustomerFeatureRelation>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			public CustomerFeatureRelation call(Row date) throws Exception {
			 SimpleDateFormat dt = new SimpleDateFormat("dd/MM/yy");
				java.util.Date lastuseDate = dt.parse(date.getString(2));
				Date lastuseDate_sql = new Date(lastuseDate.getTime());
				int customer = Integer.parseInt(date.getString(0));
				int feature = Integer.parseInt(date.getString(1));
				double frequencyuse = Double.parseDouble(date.getString(3));
				
				CustomerFeatureRelation cfr = new CustomerFeatureRelation();
				cfr.setcustomer(customer);
				cfr.setFeature(feature);
				cfr.setLastUse(lastuseDate_sql);
				cfr.setFrequencyUse(frequencyuse);
				return cfr;
			}
			   								
		   },Encoders.bean(CustomerFeatureRelation.class));
		   dateDiff.createOrReplaceTempView("dateDiff");
		  
		 Dataset<Row> customers_distinct = spark.sql("select distinct(customer) from customer");
		 List<Row> customer_list = customers_distinct.collectAsList();

		 Dataset<Row> sqlDF = spark.emptyDataFrame();
		
		 double fmeasure_total_similarity = 0.0;
		 double fmeasure_total_mf = 0.0;
		 double fmeasure_total_random = 0.0;
		 
		 Dataset<Row> weight = spark.sql("select customer,feature,lastUse,frequencyUse,(frequencyUse/datediff(current_date(),lastUse)) as weight from dateDiff");
		 
		 UserSimilarity user_sim = new UserSimilarity();
		 
		 for(int i=0;i<5;i++){
			// int customer = 6;
		 int customer = Integer.parseInt(customer_list.get(i).get(0).toString());
			 System.out.print(" customer="+customer);
			 
		 String customer_same_region_segment = "SELECT * FROM customer where segment in(select segment from customer where customer="+customer+") and region in(select region from customer where customer="+customer+")";
		 sqlDF = spark.sql(customer_same_region_segment);
////		 sqlDF.show();
		 fmeasure_total_similarity += 
				 user_sim.userSimilarity(dateDiff, spark, customer, sqlDF, dataset2);
		 RDD<MatrixEntry> rating = weight
					.join(sqlDF, sqlDF.col("customer").equalTo(weight.col("customer"))).drop(sqlDF.col("CUSTOMER")).drop(sqlDF.col("SEGMENT")).drop(sqlDF.col("REGION")).drop(weight.col("lastUse")).drop(weight.col("frequencyUse"))

					.map(new MapFunction<Row,MatrixEntry>(){

						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

						public MatrixEntry call(Row value) throws Exception {
							// TODO Auto-generated method stub
							long user = Long.parseLong(value.get(0).toString());
							long feature = Long.parseLong(value.get(1).toString());
							double rating = Double.parseDouble(value.get(2).toString());

							return new MatrixEntry(user,feature,rating);
						}
				   		
				   	}, Encoders.javaSerialization(MatrixEntry.class))
				   	.rdd();
		 RDD<Row> rating_matrix = rating.toJavaRDD()
				.map(new Function<MatrixEntry,Row>(){

					/**
					 * 
					 */
					private static final long serialVersionUID = 1L;

					public Row call(MatrixEntry v1) throws Exception {
						// TODO Auto-generated method stub
						Row values = RowFactory.create(v1.i(),v1.j(),v1.value());
						return values;
					}
					
				}).rdd();
		
		BrisMF brismf = new BrisMF();
		fmeasure_total_mf += brismf.brisMF(rating_matrix, spark, customer);
		
		RandomRecommender rand = new RandomRecommender();
		fmeasure_total_random += rand.randomRec(customer, spark, dataset2, dataset1);
		 }
		 //double fmeasure_avg_similarity = fmeasure_total_similarity/customer_list.size();
		 System.out.print("fmeasure_avg_similarity="+fmeasure_total_similarity/5+" fmeasure_avg_brismf="+fmeasure_total_mf/5+" fmeasure_avg_random="+fmeasure_total_random/5);
		 //System.out.print("total_customer="+customers_distinct.count()+"total_features="+dataset2.count()+"total_combinations="+dataset1.count());
		 

		  }
}


		
		  

