package impl;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import bean.CustomerFeatureRelation;
import bean.FeatureDetails;

public class RandomRecommender {
	
	public double randomRec(int customer, SparkSession spark, Dataset<FeatureDetails> dataset2, Dataset<CustomerFeatureRelation> dataset1){
		
		Dataset<Row> target_partial_configuration = dataset1.select("customer","feature").where(dataset1.col("customer").$eq$eq$eq(customer)).sample(true, 0.1);
		dataset1.createOrReplaceTempView("cust_feature");
		dataset2.createOrReplaceTempView("features");
		target_partial_configuration.createOrReplaceTempView("partial_conf");
		Dataset<Row> feature_for_recommendation = spark.sql("select feature from features where feature not in (select feature from partial_conf)");
		Dataset<Row> feature_recommendation = feature_for_recommendation.limit(10);
		feature_recommendation.createOrReplaceTempView("recommendations");
		long true_positives = spark.sql("select feature from recommendations where feature in (select feature from cust_feature where customer="+customer+")").count();
		long relevant_set = feature_for_recommendation.count();
		double fmeasure = 0.0;
		if(true_positives>0){
		double precision = true_positives / 10;
		double recall = true_positives / relevant_set;
		 fmeasure = 2*precision*recall/(precision+recall);
		}
		
		return fmeasure;
		
	}
}
