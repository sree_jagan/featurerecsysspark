package impl;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import bean.CustomerFeatureRelation;
import bean.FeatureDetails;
import bean.UserUserSimilarity;

public class UserSimilarity implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public double userSimilarity(Dataset<CustomerFeatureRelation> dateDiff, SparkSession spark, int customer, Dataset<Row> sqlDF, Dataset<FeatureDetails> dataset2){
		
		   dateDiff.createOrReplaceTempView("dateDiff");
		   
		   
			Dataset<CustomerFeatureRelation> partial_feature_set = dateDiff.map(new MapFunction<CustomerFeatureRelation,CustomerFeatureRelation>(){

				private static final long serialVersionUID = 1L;

				public CustomerFeatureRelation call(CustomerFeatureRelation date) throws Exception {
					// TODO Auto-generated method stub
					java.util.Date lastuseDate = date.getLastUse();
					Date lastuseDate_sql = new Date(lastuseDate.getTime());
					int customer = date.getCustomer();
					int feature = date.getFeature();
					double frequencyuse = date.getFrequencyUse();
					
					CustomerFeatureRelation cfr = new CustomerFeatureRelation();
					cfr.setcustomer(customer);
					cfr.setFeature(feature);
					cfr.setLastUse(lastuseDate_sql);
					cfr.setFrequencyUse(frequencyuse);
					return cfr;
				}
				   								
			   },Encoders.bean(CustomerFeatureRelation.class));
				
			partial_feature_set.createOrReplaceTempView("partial_feature_set");
			
			dateDiff.except(partial_feature_set.sample(true, 0.9)).createOrReplaceTempView("last_use_Date");
				   
		   Dataset<Row> weight = spark.sql("select customer,feature,lastUse,frequencyUse,(frequencyUse/datediff(current_date(),lastUse)) as weight from last_use_Date");
				   						//.as(Encoders.LONG());
		   		weight.createOrReplaceTempView("rating_weight");
		   RDD<MatrixEntry> rating = weight
				   					.join(sqlDF, sqlDF.col("customer").equalTo(weight.col("customer"))).drop(sqlDF.col("CUSTOMER")).drop(sqlDF.col("SEGMENT")).drop(sqlDF.col("REGION")).drop(weight.col("lastUse")).drop(weight.col("frequencyUse"))

				   					.map(new MapFunction<Row,MatrixEntry>(){

				   						/**
				   						 * 
				   						 */
				   						private static final long serialVersionUID = 1L;

										public MatrixEntry call(Row value) throws Exception {
				   							// TODO Auto-generated method stub
											long user = Long.parseLong(value.get(0).toString());
											long feature = Long.parseLong(value.get(1).toString());
											double rating = Double.parseDouble(value.get(2).toString());

				   							return new MatrixEntry(user,feature,rating);
				   						}
				   				   		
				   				   	}, Encoders.javaSerialization(MatrixEntry.class))
				   				   	.rdd();
		   //rating.saveAsTextFile("/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/src/main/resources/interim_output/matrix_rating");
		   //rating.saveAsTextFile("/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/src/main/resources/interim_output/matrixentry_rdd");
		   //RowMatrix ratings = new RowMatrix(rating);
		   CoordinateMatrix ratings = new CoordinateMatrix(rating);
		   //ratings.transpose().entries().saveAsTextFile("/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/src/main/resources/interim_output/coordinate_matrix_transpose");
		   JavaRDD<MatrixEntry> sim_calculation = ratings.transpose().toRowMatrix().columnSimilarities(0.1).entries().toJavaRDD();
		   JavaRDD<String> output = sim_calculation.map(new Function<MatrixEntry, String>() {
			    /**
			 * 
			 */
			private static final long serialVersionUID = 1L;

				public String call(MatrixEntry e) {
			        return String.format("%d,%d,%s", e.i(), e.j(), e.value());
			    }
			});
		   
		   
		   Dataset<String> user_similarity_string = spark.createDataset(output.rdd(), Encoders.STRING());
			
											
			user_similarity_string.show();
		   
			
			Dataset<UserUserSimilarity> user_similarity = user_similarity_string.map(new MapFunction<String,UserUserSimilarity>(){
				/**
				 * 
				 */
				private static final long serialVersionUID = 2078482594679402828L;

				public UserUserSimilarity call(String value) throws Exception {
					// TODO Auto-generated method stub
					String[] row = value.split(",");
					int userId1 = Integer.parseInt(row[0].toString());
					int userId2 = Integer.parseInt(row[1].toString());
					double similarity = Double.parseDouble(row[2].toString());
					UserUserSimilarity user_similarity = new UserUserSimilarity();
					user_similarity.setUserID1(userId1);
					user_similarity.setUserID2(userId2);
					user_similarity.setUsersSimilarity(similarity);
					return user_similarity;
					
				}
				
			}, Encoders.bean(UserUserSimilarity.class))
			;
			
			
			user_similarity.createOrReplaceTempView("userSimilarity");
			String similar_customers = "select * from userSimilarity where userID1="+customer+" order by usersSimilarity desc";
			Dataset<UserUserSimilarity> single_user_similarity = spark.sql(similar_customers)
																	.as(Encoders.bean(UserUserSimilarity.class));
			
			single_user_similarity.show(10);
//
			single_user_similarity.createOrReplaceTempView("similar_users");
			dataset2.createOrReplaceTempView("feature_details");
			
			Dataset<Row> set_of_features = spark.sql("select feature from customer_feature_relation where customer in (select userID2 from similar_users limit 10)");
			set_of_features.createOrReplaceTempView("set_of_features");
			
			String feature_for_recommendation = "select feature from set_of_features where feature not in(select feature from dateDiff where customer ="+customer+" and feature not in (select feature from last_use_Date where customer ="+customer+"))";
			Dataset<Row> complete_features = spark.sql(feature_for_recommendation);
			
			complete_features.createOrReplaceTempView("complete_features");
			
			dataset2.createOrReplaceTempView("feature_details");
			String user_recommendation = "select feature,count(feature) as tot from complete_features group by feature order by tot desc";
			Dataset<Row> feature_count = spark.sql(user_recommendation);
			feature_count.show();
			
			feature_count.createOrReplaceTempView("feature_recommendation");
			
			Dataset<Row> final_recommendations = spark.sql("select fd.feature,((fr.tot)*(fd.profit))/(fd.price) as weight from feature_details fd,feature_recommendation fr where fd.feature=fr.feature order by weight desc limit 10");
			final_recommendations.show();
			final_recommendations.createOrReplaceTempView("final_recommendations");
			
			Dataset<Row> relative_set = spark.sql("select * from rating_weight where customer="+customer);
			Dataset<Row> true_positives = spark.sql("select feature from final_recommendations where feature in (select feature from rating_weight where customer="+customer+")");
			double true_positive_features = new BigDecimal(true_positives.count()).doubleValue();
		
			System.out.print("true positives="+true_positive_features+" relative_set="+relative_set.count());
		
			double precision = true_positive_features/final_recommendations.count();
			double recall = true_positive_features/relative_set.count();
			double FMeasure = (2*precision*recall)/(precision+recall);
			System.out.println(" precision="+precision+" recall="+recall+" f-measure="+FMeasure+" completeness=0.5");
			return FMeasure;
	}
}
