package models;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.mllib.linalg.distributed.CoordinateMatrix;
import org.apache.spark.mllib.linalg.distributed.MatrixEntry;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import bean.FeatureDetails;
import bean.UserUserSimilarity;

public class KNNModel {

	static SparkConf conf = new SparkConf()
				.setMaster("local[*]");
	static SparkSession spark = SparkSession
				.builder()
				.appName("JavaKnnRecommender")
				.config(conf)
				.getOrCreate();
	private int target_customer,no_of_recommendations;
	
	private Dataset<Row> configuration_matrix;
	
	public int getTargetCustomer(){
		return target_customer;
	}
	
	public void setTargetCustomer(int target_customer){
		this.target_customer = target_customer;
	}
	
	public int getNoOfRecommendations(){
		return no_of_recommendations;
	}
	
	public void setNoOfRecommendations(int no_of_recommendations){
		this.no_of_recommendations = no_of_recommendations;
	}
	
	public Dataset<Row> getConfigurationMatrix(){
		return configuration_matrix;
	}
	
	public void setConfigurationMatrix(Dataset<Row> configuration_matrix){
		this.configuration_matrix = configuration_matrix;
	}
	
	public void run(Dataset<Row> weight,Dataset<Row> sqlDF,Dataset<FeatureDetails> dataset2){
		RDD<MatrixEntry> rating = weight
					.join(sqlDF, sqlDF.col("customer").equalTo(weight.col("customer"))).drop(sqlDF.col("CUSTOMER")).drop(sqlDF.col("SEGMENT")).drop(sqlDF.col("REGION")).drop(weight.col("lastUse")).drop(weight.col("frequencyUse"))

					.map(new MapFunction<Row,MatrixEntry>(){

						/**
						 * 
						 */
						private static final long serialVersionUID = 1L;

					@SuppressWarnings("unchecked")
					public MatrixEntry call(Row value) throws Exception {
							// TODO Auto-generated method stub
						long user = Long.parseLong(value.get(0).toString());
						long feature = Long.parseLong(value.get(1).toString());
						double rating = Double.parseDouble(value.get(2).toString());

							return new MatrixEntry(user,feature,rating);
						}
				   		
				   	}, Encoders.javaSerialization(MatrixEntry.class))
				   	.rdd();
		//rating.saveAsTextFile("/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/src/main/resources/interim_output/rowmatrix_rating");
		//rating.saveAsTextFile("/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/src/main/resources/interim_output/matrixentry_rdd");
		//RowMatrix ratings = new RowMatrix(rating);
		CoordinateMatrix ratings = new CoordinateMatrix(rating);
		//ratings.transpose().entries().saveAsTextFile("/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/src/main/resources/interim_output/coordinate_matrix_transpose");
		JavaRDD<MatrixEntry> sim_calculation = ratings.transpose().toRowMatrix().columnSimilarities(0.1).entries().toJavaRDD();
		JavaRDD<String> output = sim_calculation.map(new Function<MatrixEntry, String>() {
			public String call(MatrixEntry e) {
				return String.format("%d,%d,%s", e.i(), e.j(), e.value());
			}
		});

		String user_similarity_path = System.getProperty("user.dir")+"/resources/similar_users/customer__"+target_customer;
		File f = new File(user_similarity_path);
		try {
			FileUtils.cleanDirectory(f);
			FileUtils.forceDelete(f);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		output.saveAsTextFile(user_similarity_path);

		Dataset<String> user_similarity_string = spark
				.read()
				.option("header", false)
				//.option("delimiter", ",")
				.textFile(user_similarity_path+"/part-*")
				;

		//user_similarity_string.show();
		//similarity.show(10);

		Dataset<UserUserSimilarity> user_similarity = user_similarity_string.map(new MapFunction<String,UserUserSimilarity>(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 2078482594679402828L;

			public UserUserSimilarity call(String value) throws Exception {
				// TODO Auto-generated method stub
				String[] row = value.split(",");
				int userId1 = Integer.parseInt(row[0].toString());
				int userId2 = Integer.parseInt(row[1].toString());
				double similarity = Double.parseDouble(row[2].toString());
				UserUserSimilarity user_similarity = new UserUserSimilarity();
				user_similarity.setUserID1(userId1);
				user_similarity.setUserID2(userId2);
				user_similarity.setUsersSimilarity(similarity);
				return user_similarity;

			}

		}, Encoders.bean(UserUserSimilarity.class))
		;

		//user_similarity.show();
		user_similarity.createOrReplaceTempView("userSimilarity");
		String similar_customers = "select * from userSimilarity where userID1="+target_customer+" order by usersSimilarity desc";
		Dataset<UserUserSimilarity> single_user_similarity = spark.sql(similar_customers)
				.as(Encoders.bean(UserUserSimilarity.class));

		single_user_similarity.show(10);

		//single_user_similarity.write().save("/home/rjsreeram91/git/FeatureRecSysSpark/feature-recSys/src/main/resources/interim_output/knn_users");;
		single_user_similarity.createOrReplaceTempView("similar_users");
		dataset2.createOrReplaceTempView("feature_details");
		//Dataset<Row> userID2 = spark.sql("select userID2 from similar_users limit 10");
		//userID2.show();
		Dataset<Row> set_of_features = spark.sql("select feature from customer_feature_relation where customer in (select userID2 from similar_users limit 10)");
		set_of_features.createOrReplaceTempView("set_of_features");
		//Dataset<Row> set_of_features_ = spark.sql("select feature from set_of_features");
		//long feature_count =set_of_features.count();
		//System.out.print("\n"+ feature_count +"\n");
		String feature_for_recommendation = "select feature from set_of_features where feature not in(select feature from customer_feature_relation where customer ="+target_customer+")";
		Dataset<Row> complete_features = spark.sql(feature_for_recommendation);
		//not_set_of_features.createOrReplaceTempView("not_set_of_features");
		//Dataset<Row> distinct_customerless_feature = spark.sql("select feature from set_of_features");
		//Dataset<Row> distinct_customerless_feature_not = spark.sql("select feature from not_set_of_features");
		//not_set_of_features_.show();
		//Dataset<Row> complete_features = distinct_customerless_feature.except(distinct_customerless_feature_not);
		//complete_features.show();
		////complete_features.select("feature").distinct().createOrReplaceTempView("complete_features");;
		complete_features.createOrReplaceTempView("complete_features");
		//int feature_recommendation_limit = 10;
		String user_recommendation = "select feature,count(feature) as tot from complete_features group by feature order by tot desc limit "+no_of_recommendations;
		Dataset<Row> feature_count = spark.sql(user_recommendation);
		feature_count.show();
		System.out.print("project_directory="+System.getProperty("user.dir"));

	}
}
